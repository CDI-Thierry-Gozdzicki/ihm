package object;

import java.util.ArrayList;

import org.junit.Test;

import com.ihm.object.Figure;
import com.ihm.object.Point;

import junit.framework.TestCase;

public class FigureTest extends TestCase {
	ArrayList<Point> listPoint = null;
	float epaisseur = 2;
	boolean typeTrait = true;
	String couleur = null;

	@Test
	public void test_type() throws Exception {
		assertNotNull(Figure.class);
	}

	@Test
	public void test_instantiation() throws Exception {
		Figure target = new Figure();
		assertEquals(target, new Figure());
	}

	@Test
	public void test_creation_Point_A$String$int$int() throws Exception {
		String n = "point a";
		int a = 200;
		int b = 400;
		Point target = new Point(n, a, b);
		assertEquals(target, new Point("point a", 200, 400));
	}

	@Test
	public void test_ajoutPoint_A$int$String$int$int() throws Exception {
		Figure target = new Figure();
		String n = "point a";
		int p = 1;
		int a = 200;
		int b = 400;
		target.ajoutPoint(p, n, a, b);
	}

	public void test_suppressionPoint_A$String$int$int$int() throws Exception {
		Figure target = new Figure();
		String n = "point a";
		int a = 200;
		int b = 400;
		int p = 1;
		target.ajoutPoint(p, n, a, b);
		target.suppressionPoint(p - 1);
		assert (target.listPoint.isEmpty());
	}
}