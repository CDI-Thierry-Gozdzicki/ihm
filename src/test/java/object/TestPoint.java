package object;

import org.junit.Test;

import com.ihm.object.Point;

import junit.framework.TestCase;

public class TestPoint extends TestCase {
	@Test
	public void test_type() throws Exception {
		assertNotNull(Point.class);
	}

	@Test
	public void test_instantiation() throws Exception {
		Point target = new Point();
		assertNotNull(target);
	}

	@Test
	public void test_deplacer_A$int$int() throws Exception {
		Point target = new Point();
		int dx = 5;
		int dy = 02;
		target.deplacer(dx, dy);
	}
}
