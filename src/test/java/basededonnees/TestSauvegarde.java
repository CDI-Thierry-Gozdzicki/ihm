package basededonnees;

import static org.junit.Assert.assertTrue;
import java.io.File;
import org.junit.Before;
import org.junit.Test;
import com.ihm.basededonnees.Sauvegarde;
import com.ihm.basededonnees.ValidationXsd;
import com.ihm.config.Constantes;
import com.ihm.object.Figure;
import com.ihm.object.Projet;

/**
 * La classe testSauvegarde verifie la création du fichier et le format
 * d'ecriture.
 * 
 * figure est une figure.
 * projet est un projet.
 * xml est le fichier de sauvegarde.
 * xsd est le fichier xsd de verification.
 * 
 * @author Thierry
 */

public class TestSauvegarde {

	private static Figure figure;
	private static Projet projet;
	private static File xml;
	private static File xsd;

	/*
	 * Methode qui permet l'initialisation des parametres necessaire au test.
	 */

	@Before
	public void setupBeforClass() {

		// Creation d'un projet et d'une figure
		projet = new Projet();
		figure = new Figure();

		// Creation de trois point et ajout dans la TreeMap de la figure
		figure.ajoutPoint(1, "A", 100, 100);
		figure.ajoutPoint(2, "B", 200, 200);
		figure.ajoutPoint(3, "C", 300, 300);

		// Ajout de la figure dans l'ArrayList du projet
		projet.ajoutFigure(figure);

		// Creation du fichier
		xml = new File(System.getProperty("user.dir")+ File.separator + "Data" + File.separator + "testSauve.xml");
		xsd = new File(TestSauvegarde.class.getResource(Constantes.PATH_XSD).getFile());
	}

	/*
	 * Methode qui teste la creation du fichier.
	 */

	@Test
	public void should_do_when_file_exist() {
		Sauvegarde.ecritureFichier(projet, xml);
		assertTrue(xml.exists());
	}

	/*
	 * Methode qui teste le format du fichier.
	 */

	@Test
	public void should_do_when_file_format() {
		assertTrue(ValidationXsd.validation(xsd, xml) == true);
	}
}