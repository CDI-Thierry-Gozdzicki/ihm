package com.ihm.config;

public class Constantes {

	// Déclaration des constantes pour les boutons et combobox
	public final static String PROPOS = "Version 1.1: Projet realise par Brandon, Thierry et Najim";
	public final static String TRAITPOINTILLE = "- - - - - - - - - - - - -";
	public final static String TRAITCONTINU = "____________";
	public static final String PATH_FOND = "/fond.png";
	public static final String PATH_FONDNOIR = "/fondnoir.jpg";
	public static final String PATH_LOGO = "logo.png";
	public static final String PATH_POUBELLE = "/poubelle.png";
	public static final String PATH_XSD = "/projet.xsd";
	public static final String PRECEDENT = "precedent";
	public static final String SUIVANT = "suivant";
	public static final String NOUVEAU = "nouvelle figure";
	public static final String THEME_JOUR = "Jour";
	public static final String THEME_NUIT = "Nuit";

	// Déclaration des constantes pour les loggers
	public static final String LOGGER_CREATE_FIGURE = "Une figure a ete creee.";
	public static final String LOGGER_CREATE_POINT = "Un point a ete cree.";
	public static final String LOGGER_DELETE_FIGURE = "Une figure a ete supprimee.";
	public static final String LOGGER_DELETE_POINT = "Un point a ete supprime.";
	public static final String LOGGER_EXIT = "Le programme a ete quitte par l'utilisateur.";
	public static final String LOGGER_FIGURE_EMPTY = "La derniere figure est vide impossible d'un creer une nouvelle.";
	public static final String LOGGER_FIGURE_NEXT = "Affichage de la figure suivante.";
	public static final String LOGGER_FIGURE_PREVIOUS = "Affichage de la figure precedente.";
	public static final String LOGGER_SUCCESS_LOAD = "Le fichier a ete charge.";
	public static final String LOGGER_SUCCESS_SAVE = "Le fichier a ete sauvegarde.";
	public static final String LOGGER_UPDATE_COLOR = "La couleur du trait a ete modifie.";
	public static final String LOGGER_UPDATE_SIZE = "L'epaisseur du trait a ete modifie.";
	public static final String LOGGER_UPDATE_STYLE = "Le style du trait a ete modifie.";
	public static final String LOGGER_WINDOWS_STYLE = "Le style de la fenetre a ete change.";
	public static final String LOGGER_WRONG_FILE_EMPTY = "Le fichier XML est vide, ou ne contient pas de projet valide.";
	public static final String LOGGER_WRONG_FILE_SELECT = "L'utilisateur n'a pas selectionne de fichier de sauvegarde.";
	public static final String LOGGER_WRONG_FORMAT_FILE = "Le fichier ne correspond pas au format XML.";
	public static final String LOGGER_WRONG_FORMAT_XML = "Le fichier XML ne correspond pas au format de validation.";
	public static final String LOGGER_WRONG_LOAD = "Le fichier n'a pas pu etre charge.";
	public static final String LOGGER_WRONG_SAVE = "Le fichier n'a pas pu etre sauvegarde.";
	public static final String LOGGER_WRONG_COLOR = "L'utilisateur a quitte la fenetre de couleur.";
	public static final String LOGGER = "";
}