package com.ihm.pattern;

import java.util.regex.Pattern;

public class PatternChiffre {

	public static void main(String[] args) {
		System.out.println(valeurOrdAbs("123"));
	}

	
	public static boolean valeurOrdAbs(String str) {
		return Pattern.compile("(\\d+)").matcher(str).matches();
	}
}
