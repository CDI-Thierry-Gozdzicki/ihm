package com.ihm.visuel;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import com.ihm.object.Fichier;
import com.ihm.object.Point;
import com.ihm.pattern.PatternChiffre;

public class PanneauPoint extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static JButton ajouterPoint = new JButton("Ajouter un point");
	public static JButton suppimerTousLesPoints = new JButton("Supprimer tout");
	// panneau comportant les boutons ajouterPoint et SupprimerTousLesPoints
	public static JPanel panelBoutons = new JPanel();

	// panneau comportant une liste de PanneauPointLigne
	public static JPanel panneauListeDePoint = new JPanel();
	// encapsulement du panneauListeDePoint dans un panneau avec ScrollBar
	public static JScrollPane scroll = new JScrollPane(panneauListeDePoint, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);



	public PanneauPoint() {

		this.setLayout(new BorderLayout());
		;
		panneauListeDePoint.setLayout(new GridLayout(100, 0));
		
		configBoutonAjouterPoint();
		configBoutonSupprimerTousLesPoints();
		
		this.add(scroll);
		this.add(panelBoutons, BorderLayout.SOUTH);
	}

	private void configBoutonAjouterPoint() {
		panelBoutons.add(ajouterPoint);
		ajouterPoint.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boiteDialogue();
				rafraichirPoints();
			}
		});		
	}

	private void configBoutonSupprimerTousLesPoints() {
		panelBoutons.add(suppimerTousLesPoints);
		suppimerTousLesPoints.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Fichier.getFigureEnCours().getListPoint().clear();
				FenetrePrincipale.panneauDessin.repaint();
				PanneauPoint.panneauListeDePoint.removeAll();
				panneauListeDePoint.repaint();
				panneauListeDePoint.validate();
			}
		});
		
	}

	// Methode pour la creation de la boite de dialogue d'ajout de point
	public static void boiteDialogue() {

		JPanel panelBoiteDialogue = new JPanel();

		// Creation de la zone de texte de la position du point
		JTextField textPos = new JTextField();
		textPos.setText(Fichier.getFigureEnCours().listPoint.size() + 1 + "");
		textPos.setColumns(5);
		ajoutCouple(panelBoiteDialogue, new JPanel(), textPos, new JLabel("Position"));

		// Creation de la zone de texte du nom du point
		JTextField textNom = new JTextField();
		textNom.setColumns(5);
		textNom.setText(Fichier.incrementeLettre());
		ajoutCouple(panelBoiteDialogue, new JPanel(), textNom, new JLabel("Nom"));

		// Creation de la zone de texte de l'abscisse du point
		JTextField textX = new JTextField();
		textX.setColumns(5);
		ajoutCouple(panelBoiteDialogue, new JPanel(), textX, new JLabel("X"));

		// Creation de la zone de texte de l'ordonnee du point
		JTextField textY = new JTextField();
		textY.setColumns(5);
		ajoutCouple(panelBoiteDialogue, new JPanel(), textY, new JLabel("Y"));

		JOptionPane.showMessageDialog(null, panelBoiteDialogue);

		// Condition si la valeur de l'ordonnee ou de l'abscisse est un integer permet
		// la creation d'un point.
		if (PatternChiffre.valeurOrdAbs(textPos.getText()) && PatternChiffre.valeurOrdAbs(textX.getText())
				&& PatternChiffre.valeurOrdAbs(textY.getText())) {
			Fichier.getFigureEnCours().ajoutPoint(Integer.parseInt(textPos.getText()), textNom.getText(),
					Integer.parseInt(textX.getText()), Integer.parseInt(textY.getText()));
		}

		// Condition si la valeur de l'ordonne ou de l'abscisse n'est pas un integer,
		// renvoie un massage d'erreur.
		else {
			JOptionPane.showMessageDialog(null, "Veuillez entrer des coordonnees correctes", "Erreur",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	// Methode pour la creation de la zone de texte
	private static void ajoutCouple(JPanel dial, JPanel couple, JTextField text, JLabel lab) {
		couple.add(lab);
		couple.add(text);
		dial.add(couple);
	}

	public static void rafraichirPoints() {

		panneauListeDePoint.removeAll();
		for (Point entree : Fichier.getFigureEnCours().listPoint) {
			panneauListeDePoint.add(new PanneauPointLigne(Fichier.getFigureEnCours().listPoint.indexOf(entree),
					entree.getNom(), entree.getAbscisse(), entree.getOrdonnee()));
		}
		panneauListeDePoint.repaint();
	}
}