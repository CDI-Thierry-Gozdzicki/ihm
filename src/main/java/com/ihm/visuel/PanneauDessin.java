package com.ihm.visuel;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import com.ihm.config.Constantes;
import com.ihm.object.Fichier;
import com.ihm.object.Point;


/**
 * Classe utilisé pour dessiner
 * 
 * @author Team 2
 *
 */


public class PanneauDessin extends JPanel {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	private  static boolean darkMode = false;
	private static Color rouge = new Color(0.5f, 0, 0, 0.1f); //Red 
	private static Color gris = new Color(0.682f, 0.678f, 0.678f, 0.1f); //Red 


	/*
	 * Constructeur
	 */
	public PanneauDessin() {
		
		this.setBackground(Color.white);
		// set bordure noir d'épaisseur 2px
		this.setBorder(BorderFactory.createLineBorder(Color.black, 2));

		// ajout listener pour dessin à main levé
		this.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				Fichier.getFigureEnCours().ajoutPoint(Fichier.getFigureEnCours().getListPoint().size() + 1,Fichier.incrementeLettre(), e.getX(), e.getY());		
				PanneauPoint.rafraichirPoints();
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
			}
		});
	}

	/**
	 * Methode permettant de dessiner la grille et traits
	 * @param graphics : outil utilisé pour dessiner
	 */
	@Override
	protected void paintComponent(Graphics graphics) {
		
		// en fonction du theme, defini l'arriere plan
		if(isDarkMode())
			setBackground(Color.BLACK);
		else 
			setBackground(Color.white);

		Image fondBlanc = Toolkit.getDefaultToolkit().getImage(PanneauDessin.class.getResource(Constantes.PATH_FOND));	
		Image fondNoir = Toolkit.getDefaultToolkit().getImage(PanneauDessin.class.getResource(Constantes.PATH_FONDNOIR));	

		if (!darkMode)
			graphics.drawImage(fondBlanc, 0, 0, getWidth(), getHeight(), null);
		else
			graphics.drawImage(fondNoir, 0, 0, getWidth(), getHeight(), null);


		// trace la grille arriere 
		int j = 0;
		for (int i = 10 ; i < (getWidth()>getHeight()?getWidth()+100:getHeight()+100) ; i = i + 10) {

			if (!isDarkMode())
				graphics.setColor(rouge);
			else
				graphics.setColor(gris);

			j+=10;
			if (j==100) {

				if (!isDarkMode())
					graphics.setColor(Color.GRAY);
				else
					graphics.setColor(Color.GRAY);

				graphics.drawLine(i, 0, i, getHeight());
				graphics.drawLine(0,i, getWidth(), i);

				graphics.drawString(i+"", i+2, 15 );
				graphics.drawString(i+"", i+2, getHeight() - 5 );

				graphics.drawString(i+"", 3, i-2 );
				graphics.drawString(i+"", getWidth() - 25 , i - 2 );
				j=0;
			} else  {
				graphics.drawLine(i, 0, i, getHeight());
				graphics.drawLine(0,i, getWidth(), i);

			}
		}
		graphics.setColor(Color.GRAY);		

		// récupere la liste des points
		ArrayList<Point> list = new ArrayList<Point>();
		list = Fichier.getFigureEnCours().listPoint;

		// transforme graphics en graphics 2D pour pouvoir tracer en pointille
		Graphics2D g1 = (Graphics2D) graphics;
		BasicStroke line = new BasicStroke(Fichier.getFigureEnCours().getEpaisseur());
		// défini le style de pointille
		Stroke dashed = new BasicStroke(Fichier.getFigureEnCours().getEpaisseur(), BasicStroke.CAP_BUTT,
				BasicStroke.JOIN_BEVEL, 0, new float[] { 9 }, 0);


		// definie le mode de tracé
		if (Fichier.getFigureEnCours().isTypeTrait())
			g1.setStroke(dashed);
		else
			g1.setStroke(line);


		g1.setColor(Fichier.getFigureEnCours().retourColor(Fichier.getFigureEnCours().getCouleur()));

		// ajout des points avec saisie manuelle
		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				if (i < list.size() - 1) {
					int x1 = list.get(i).getAbscisse();
					int y1 = list.get(i).getOrdonnee();
					int x2 = list.get(i + 1).getAbscisse();
					int y2 = list.get(i + 1).getOrdonnee();
					g1.drawString(list.get(i).getNom(), x1>x2 ? x1+7 : x1-10, y1>y2 ? y1+7:y1-7);
					g1.drawLine(x1, y1, x2, y2);
				} else {
					int x1 = list.get(i).getAbscisse();
					int y1 = list.get(i).getOrdonnee();
					int x2 = list.get(0).getAbscisse();
					int y2 = list.get(0).getOrdonnee();
					g1.drawString(list.get(i).getNom(),  x1>x2?x1+7:x1-7, y1>y2 ? y1+7:y1-7);
					g1.drawLine(x1, y1, x2, y2);
				}
			}
		}
	}

	public static boolean isDarkMode() {
		return darkMode;
	}

	public static void setDarkMode(boolean darkMode) {
		PanneauDessin.darkMode = darkMode;
	}


}