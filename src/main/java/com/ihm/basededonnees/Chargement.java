package com.ihm.basededonnees;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ihm.basededonnees.ValidationXsd;
import com.ihm.config.Constantes;
import com.ihm.object.Projet;

/**
 * La classe chargement importe les données d'un fichier XML.
 * Elle permet de charger la sauvegarde d'un projet qui est sous format XML.
 * 
 * jaxb est un JAXBContext.
 * unmarsh est un Unmarshaller. 
 * projet est le Projet contenant la sauvegarde.
 * xsd est le fichier de validation.
 * logger est le Logger pour la recuparation d'erreur.
 * 
 * @author Thierry
 */

public class Chargement {

	// Déclaration des variables
	private static JAXBContext jaxb;
	private static Unmarshaller unmarsh;
	private static Projet projet;
	private static File xsd;
	private static final Logger logger = LoggerFactory.getLogger(Chargement.class);

	/**
	 * Méthode lectureFichier permet de charger un projet sauvegardé
	 * dans un fichier choisi par l'utilisateur.
	 * 
	 * @param xml contient le fichier choisi par l'utilisateur.
	 * @return le projet créé grace a la sauvegarde.
	 */
	
	public static Projet lectureFichier(File xml) {

		// Création du fichier de validation
		xsd = new File(Chargement.class.getResource(Constantes.PATH_XSD).getFile());

		// Condition si le fichier chargé correspond au schéma de validation
		if (ValidationXsd.validation(xsd, xml)) {

			try {
				// Création d'un objet de type JAXBContext et d'un objet Unmarshaller
				jaxb = JAXBContext.newInstance(Projet.class);
				unmarsh = jaxb.createUnmarshaller();

				// Création d'un projet contenant les valeurs du fichier
				projet = (Projet) unmarsh.unmarshal(xml);

			} catch (JAXBException e) {
				logger.error(Constantes.LOGGER_WRONG_LOAD);
			}
			return projet;
		}

		// Condition si le fichier chargé ne correspond pas au schéma de validation
		else {
			logger.warn(Constantes.LOGGER_WRONG_FORMAT_XML);
			return null;
		}
	}
}