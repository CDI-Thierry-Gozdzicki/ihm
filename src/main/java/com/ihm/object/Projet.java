package com.ihm.object;

import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import com.ihm.visuel.PanneauBouton;
import lombok.Data;

/**
 * Class qui permet la création d'un projet stockant les figures.
 * 
 * mapFigure qui contient les figures du projet.
 * 
 * @author Thierry
 */

@XmlRootElement(name = "projet")
@XmlAccessorType(XmlAccessType.FIELD)
@Data
public class Projet {

	// Déclaration des attributs
	private ArrayList<Figure> listFigure;

	/**
	 * Constructeur par defaut avec la creation de la HashMap
	 */

	public Projet() {
		this.listFigure = new ArrayList<>();
	}
	
	/**
	 * Méthode ajoutFigure sans paramètre, elle permet d'ajouter
	 * une nouvelle figure vide au projet en cours.
	 * 
	 */
	
	public void ajoutFigure() {
		
		Figure f = new Figure();
		listFigure.add(f);
		Fichier.setFigureEnCours(f);
		PanneauBouton.validationFenetre();
	}
	
	/**
	 * Méthode qui permet l'ajout d'une figure dans la liste du projet.
	 * 
	 * @param figure contient la figure à ajouter.
	 */
	
	public void ajoutFigure(Figure figure) {
		listFigure.add(figure);
	}
}