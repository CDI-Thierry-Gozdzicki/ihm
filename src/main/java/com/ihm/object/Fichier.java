package com.ihm.object;

import java.util.ArrayList;
import com.ihm.config.Constantes;
import com.ihm.visuel.FenetrePrincipale;
import lombok.Data;

/**
 * Class qui permet la création d'une session.
 * 
 * projet qui est le projet en cours.
 * listFigure qui est la liste des figures du projet.
 * figureEnCours qui est la figure en cours.
 * indexFigureEnCours qui est l'index de la figure dans la liste.
 * 
 * @author Thierry et Najim
 */

@Data
public class Fichier {

	private static Projet projet;
	private static ArrayList<Figure> listFigure;
	private static Figure figureEnCours;
	private static int indexFigureEncours = 0;

	/*
	 * Fichier Constructeur par défaut.
	 */

	public Fichier() {
		Fichier.projet = new Projet();
	}
	
	/**
	 * Methode newProject permet de réinitialiser le projet
	 */
	
	public static void newProject() {
		
		// Réinitialisation du projet et remise à zéro des valeurs
		setProjet(new Projet());
		getProjet().getListFigure().clear();
		getProjet().getListFigure().add(new Figure());
		Fichier.figureEnCours = getProjet().getListFigure().get(0);
		
		// Rafraichissement de l'interface visuel
		FenetrePrincipale.panneauDessin.repaint();
		FenetrePrincipale.panneauDessin.revalidate();
		FenetrePrincipale.panneauBouton.validate();
	}
	
	/**
	 * Méthode choixFigure qui permet de naviger entre les différentes figure de la
	 * liste.
	 * 
	 * @param str contient l'indication suivante ou précédante.
	 */

	public static void choixFigure(String str) {

		// Récupération de la liste de figure
		listFigure = projet.getListFigure();

		// Condition si la liste est supérieur à un
		if (listFigure.size() > 1) {

			// Condition si l'utilisateur veut la figure suivante
			if (str.equalsIgnoreCase(Constantes.PRECEDENT)) {

				// Condition si la figure n'est pas a l'index 0
				if (indexFigureEncours != 0) {
					indexFigureEncours--;
				} else {
					indexFigureEncours = listFigure.size() - 1;
				}
			}

			// Condition si l'utilisateur veut la figure precedente
			else if (str.equalsIgnoreCase(Constantes.SUIVANT)) {

				// Condition si la figure n'est pas au dernier index
				if (indexFigureEncours != listFigure.size() - 1) {
					indexFigureEncours++;
				} else {
					indexFigureEncours = 0;
				}
			}
			figureEnCours = listFigure.get(indexFigureEncours);
		}
	}

	/**
	 * Methode incrementeLettre permet d'ajouter par défaut dans le champs de texte
	 * d'un nouveau point la lettre suivant la précédente.
	 * 
	 * @return le nom du futur point.
	 */

	public static String incrementeLettre() {

		// Condition si la liste est vide retourne le caractère 'A'
		if (getFigureEnCours().getListPoint().isEmpty()) {
			return "A";
		}

		// Recupération du String et création d'un StringBuilder
		String s = getFigureEnCours().getListPoint().get(getFigureEnCours().getListPoint().size() - 1).getNom();
		StringBuilder res = new StringBuilder();

		// Condition si le nom du point ne contient qu'un seul caractère
		if (s.length() == 1) {
			
			//Condition si le nom est 'Z'
			if (s.equalsIgnoreCase("Z")) {
				res.append("AA");
			} else {
				res.append((char)((int)s.charAt(0) + 1));
			}
		}
		
		// Condition si le le nom point contient plusieurs caractères
		else {
			
			// Condition si le dernier caractère est 'Z'
			if (s.charAt(s.length() -1) == 'Z') {
				res.append((char)((int)s.charAt(0) + 1));
				res.append("A");
			} else {
				res.append(s.charAt(0));
				res.append((char)((int)s.charAt(1) + 1));
			}
		}
		return res.toString();
	}

	public static Projet getProjet() {
		return projet;
	}

	public static void setProjet(Projet projet) {
		Fichier.projet = projet;
	}

	public static Figure getFigureEnCours() {
		return figureEnCours;
	}

	public static void setFigureEnCours(Figure figureEnCours) {
		Fichier.figureEnCours = figureEnCours;
	}

	public static int getIndexFigureEncours() {
		return indexFigureEncours;
	}

	public static void setIndexFigureEncours(int indexFigureEncours) {
		Fichier.indexFigureEncours = indexFigureEncours;
	}
}